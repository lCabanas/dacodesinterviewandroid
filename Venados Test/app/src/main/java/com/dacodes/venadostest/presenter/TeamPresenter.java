package com.dacodes.venadostest.presenter;

import android.content.Context;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.data.service.ClientService;
import com.dacodes.venadostest.data.service.response.TeamResponse;
import com.dacodes.venadostest.data.service.response.base.BaseResponse;
import com.dacodes.venadostest.views.viewcontrollers.TeamViewController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TeamPresenter {
    private TeamViewController mViewController;
    private ClientService mClientService;
    private Context mContext;

    public TeamPresenter(TeamViewController mViewController, Context mContext) {
        this.mViewController = mViewController;
        this.mContext = mContext;
        if (this.mClientService == null) {
            this.mClientService = new ClientService();
        }
    }

    public void getTeamWS(){
        mViewController.showProgress();
        mClientService
                .getAPI()
                .players()
                .enqueue(new Callback<BaseResponse<TeamResponse>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<TeamResponse>> call, Response<BaseResponse<TeamResponse>> response) {
                        BaseResponse<TeamResponse> mResponse = response.body();
                        mViewController.dissmissProgress();
                        if (mResponse != null) {
                            if (mResponse.success && mResponse.data.code == 200) {
                                mViewController.successObjctWS(mResponse.data.team);
                            } else {
                                mViewController.emptyTeam();
                            }
                        } else {
                            mViewController.showMessage(mContext.getString(R.string.message_error));
                            mViewController.emptyTeam();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<TeamResponse>> call, Throwable t) {
                        mViewController.dissmissProgress();
                        mViewController.showMessage(mContext.getString(R.string.message_error));
                        mViewController.emptyTeam();
                    }
                });
        mViewController.dissmissProgress();
    }
}
