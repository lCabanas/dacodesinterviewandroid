package com.dacodes.venadostest.data.service.response;

import com.dacodes.venadostest.model.Game;

import java.util.List;


public class GameResponse {
    public List<Game> games;
    public int code;
}
