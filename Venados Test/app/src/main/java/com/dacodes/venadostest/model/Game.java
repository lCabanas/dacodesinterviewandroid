package com.dacodes.venadostest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Game {

    public boolean local;

    public String opponent;

    @SerializedName("opponent_image")
    public String url_opponent_image;

    public Date datetime;

    public String league;

    @SerializedName("image")
    public String url_image;

    public int home_score;

    public int away_score;
}
