package com.dacodes.venadostest.data.service;

import com.dacodes.venadostest.data.service.api.ClientAPI;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ClientService {
    private Retrofit mRetrofit = null;

    public static final String URL_API = "https://venados.dacodes.mx/api/";
    public static final String URL_LOGO = "https://s3.amazonaws.com/lmxwebsite/docs/archdgtl/AfldDrct/logos/10732/10732.png";
    public ClientAPI getAPI() {
        if (mRetrofit == null) {

            OkHttpClient client = new OkHttpClient
                    .Builder().connectTimeout(10, TimeUnit.HOURS)
                    .writeTimeout(10, TimeUnit.HOURS)
                    .readTimeout(10, TimeUnit.HOURS)
                    .build();


            Gson gson = new GsonBuilder()
                    .setDateFormat("yyyy-MM-dd HH:mm:ss")
                    .serializeNulls()
                    .create();


            mRetrofit = new Retrofit
                    .Builder()
                    .baseUrl(URL_API)
                    .addConverterFactory(GsonConverterFactory.create(gson))
                    .client(client)
                    .build();
        }

        return mRetrofit.create(ClientAPI.class);
    }
}
