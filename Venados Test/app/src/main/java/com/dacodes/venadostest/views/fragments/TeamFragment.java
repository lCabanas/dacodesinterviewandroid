package com.dacodes.venadostest.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.model.Team;
import com.dacodes.venadostest.presenter.TeamPresenter;
import com.dacodes.venadostest.views.fragments.base.BaseFragment;
import com.dacodes.venadostest.views.viewcontrollers.TeamViewController;
import com.dacodes.venadostest.views.widget.ViewLoadingDotsGrow;
import com.google.android.material.tabs.TabLayout;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class TeamFragment extends BaseFragment implements TeamViewController {

    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.viewPagerPlayers)
    ViewPager mViewPager;

    @BindView(R.id.textViewEmpty)
    TextView mTextViewEmpty;

    @BindView(R.id.swipeRefreshTeam)
    SwipeRefreshLayout mSwipeRefreshLayout;

    private PlayerPagerAdapter mPlayerPagerAdapter;
    private TeamPresenter mTeamPresenter;
    int currentFragment = 0;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            view = inflater.inflate(R.layout.fragment_team, container, false);
            ButterKnife.bind(this, view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        if (mTeamPresenter == null) {
            mTeamPresenter = new TeamPresenter(this, getActivity());
        }

        currentFragment = mViewPager != null ? mViewPager.getCurrentItem() : 0;
        mTeamPresenter.getTeamWS();

        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentFragment = mViewPager != null ? mViewPager.getCurrentItem() : 0;
                mPlayerPagerAdapter = null;
                mTeamPresenter.getTeamWS();
            }
        });

    }

    @Override
    public void emptyTeam() {
        mViewPager.setVisibility(View.GONE);
        mTextViewEmpty.setVisibility(View.VISIBLE);
    }

    @Override
    public void showMessage(String message) {
        showToastMessage(message, getActivity());
    }

    @Override
    public void successObjctWS(Team object) {
        mViewPager.setVisibility(View.VISIBLE);
        mTextViewEmpty.setVisibility(View.GONE);
        configureSectionPager(object);
    }

    @Override
    public void successListWS(List<Team> listObject) {

    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
        mTextViewEmpty.setVisibility(View.GONE);
    }

    @Override
    public void dissmissProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private void configureSectionPager(Team mTeam){
        if (mPlayerPagerAdapter == null) {
            mPlayerPagerAdapter = new PlayerPagerAdapter(getFragmentManager(),mTeam,getActivity());
            mViewPager.setAdapter(mPlayerPagerAdapter);
            mTabLayout.setupWithViewPager(mViewPager);
        }
        mViewPager.setCurrentItem(currentFragment);
    }

    private class PlayerPagerAdapter extends FragmentStatePagerAdapter {
        private Context mContext;
        private Team mTeam;

        public PlayerPagerAdapter(FragmentManager fm, Team mTeam, Context mContext) {
            super(fm);
            this.mContext = mContext;
            this.mTeam = mTeam;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            Fragment fragment = null;
            switch (position){
                case 0:
                    fragment = PlayersFragment.newInstance(mContext,mTeam.goalkeepers);
                    break;
                case 1:
                    fragment = PlayersFragment.newInstance(mContext,mTeam.defenses);
                    break;
                case 2:
                    fragment = PlayersFragment.newInstance(mContext,mTeam.centers);
                    break;
                case 3:
                    fragment = PlayersFragment.newInstance(mContext,mTeam.forwards);
                    break;
            }
            return fragment;
        }

        @Override
        public int getCount() {
            return 4;
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            switch (position){
                case 0:
                    return "Porteros";
                case 1:
                    return "Defensas";
                case 2:
                    return "Medios";
                case 3:
                    return "Delanteros";
            }
            return null;
        }
    }

}
