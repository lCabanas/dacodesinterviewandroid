package com.dacodes.venadostest.data.service.response;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Data<T> {
    @SerializedName(value="games", alternate={"team"})
    public List<T> listData;

    @SerializedName(value="games", alternate={"team"})
    public T data;

    public int code;
}
