package com.dacodes.venadostest.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter;
import com.dacodes.venadostest.R;
import com.dacodes.venadostest.model.Game;
import com.dacodes.venadostest.views.adapters.holders.GameHeaderHolder;
import com.dacodes.venadostest.views.adapters.holders.GameHolder;
import com.dacodes.venadostest.views.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class GameAdapter extends StickyHeaderGridAdapter {
    private List<String> headers = new ArrayList<>();
    private  List<Game> mGameList;
    private List<List<Game>> mGamesItems = new ArrayList<>();
    GameHolder.OnDateClickListener mOnDateClickListener;

    public GameAdapter(List<Game> mGameList, GameHolder.OnDateClickListener mOnDateClickListener) {
        this.mGameList = mGameList;
        this.mOnDateClickListener = mOnDateClickListener;

        if(this.mGameList != null && !this.mGameList.isEmpty()){
            for (Game mGame : this.mGameList){
                String month = Utils.configureDateMonth(mGame.datetime);
                if(!headers.contains(month)){
                    this.headers.add(month);
                }
            }

            for (String header : headers){
                List<Game> mGameFiltered = new ArrayList<>();
                for (Game mGame : this.mGameList){
                    String month = Utils.configureDateMonth(mGame.datetime);
                    if(month.equals(header)){
                        mGameFiltered.add(mGame);
                    }
                }
                if(mGameFiltered != null && !mGameFiltered.isEmpty()) {
                    mGamesItems.add(mGameFiltered);
                }
            }

        }

    }

    @Override
    public int getSectionCount() {
        return headers.size();
    }

    @Override
    public int getSectionItemCount(int section) {
        return mGamesItems.get(section).size();
    }

    @Override
    public HeaderViewHolder onCreateHeaderViewHolder(ViewGroup parent, int headerType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_games_header, parent, false);
        return new GameHeaderHolder(view);
    }

    @Override
    public ItemViewHolder onCreateItemViewHolder(ViewGroup parent, int itemType) {
        final View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_game, parent, false);
        return new GameHolder(view, this.mOnDateClickListener);
    }

    @Override
    public void onBindHeaderViewHolder(HeaderViewHolder viewHolder, int section) {
        final GameHeaderHolder holder = (GameHeaderHolder) viewHolder;
        final String label = headers.get(section);
        holder.config(label);
    }

    @Override
    public void onBindItemViewHolder(ItemViewHolder viewHolder, int section, int offset) {
        final GameHolder holder = (GameHolder) viewHolder;
        List<Game> mGameList = mGamesItems.get(section);
        if(mGameList != null && !mGameList.isEmpty()){
            Game mGame = mGameList.get(offset);
            holder.config(mGame);
        }
    }
}
