package com.dacodes.venadostest.views.fragments;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.model.Player;
import com.dacodes.venadostest.views.adapters.PlayerAdapter;
import com.dacodes.venadostest.views.adapters.holders.base.BaseViewHolder;
import com.dacodes.venadostest.views.fragments.base.BaseFragment;
import com.dacodes.venadostest.views.utils.ImageUtils;
import com.dacodes.venadostest.views.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PlayersFragment extends BaseFragment {
    @BindView(R.id.recyclerViewPlayers)
    RecyclerView mRecyclerViewPlayers;

    @BindView(R.id.textViewEmpty)
    TextView mTextViewEmpty;

    Context mContext;
    List<Player> mPlayerList;
    PlayerAdapter mPlayerAdapter;

    public static PlayersFragment newInstance(Context mContext, List<Player> mPlayerList) {
        PlayersFragment mFragment = new PlayersFragment();
        mFragment.mPlayerList = mPlayerList;
        mFragment.mContext = mContext;
        return mFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        configureRecyclerView(mPlayerList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            view = inflater.inflate(R.layout.fragment_player, container, false);
            ButterKnife.bind(this, view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    private void InfoPlayerDialog(Player mPlayer){
        final Dialog dialog = new Dialog(mContext);
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE); // before
        dialog.setContentView(R.layout.dialog_info_player);
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
        dialog.setCancelable(true);

        CircleImageView mCircleImageViewPhoto = (CircleImageView) dialog.findViewById(R.id.circleImageViewPhoto);
        TextView mTextViewName = (TextView) dialog.findViewById(R.id.textViewName);
        TextView mTextViewBirthday = (TextView) dialog.findViewById(R.id.textViewBirthday);
        TextView mTextViewBirthplace = (TextView) dialog.findViewById(R.id.textViewBirthPlace);
        TextView mTextViewWeight = (TextView) dialog.findViewById(R.id.textViewWeight);
        TextView mTextViewHeight = (TextView) dialog.findViewById(R.id.textViewHeight);
        TextView mTextViewLastTeam = (TextView) dialog.findViewById(R.id.textViewLastTeam);

        if (mPlayer.url_image != null && !mPlayer.url_image.isEmpty()) {
            ImageLoader.getInstance().displayImage(mPlayer.url_image, mCircleImageViewPhoto, ImageUtils.mDisplayImageOptions);
        }
        mTextViewName.setText(mPlayer.position + "\n" + mPlayer.getFullName());
        mTextViewBirthday.setText(Utils.configureDate(mPlayer.birthday));
        mTextViewBirthplace.setText(mPlayer.birth_place);
        mTextViewWeight.setText("" + mPlayer.weight);
        mTextViewHeight.setText("" + mPlayer.height);
        mTextViewLastTeam.setText(mPlayer.last_team);
        dialog.show();
    }

    public void configureRecyclerView(List<Player> mPlayerList) {
        if(mPlayerList != null && !mPlayerList.isEmpty()) {
            PlayerAdapter mPlayerAdapter = new PlayerAdapter(mPlayerList, mOnViewHolderClickListener);
            GridLayoutManager layoutManager = new GridLayoutManager(mContext, 3);
            mRecyclerViewPlayers.setLayoutManager(layoutManager);
            mRecyclerViewPlayers.setHasFixedSize(true);
            mRecyclerViewPlayers.setItemAnimator(new DefaultItemAnimator());
            mRecyclerViewPlayers.setAdapter(mPlayerAdapter);
            mTextViewEmpty.setVisibility(View.GONE);
        }else{
            mTextViewEmpty.setVisibility(View.VISIBLE);
        }
    }

    BaseViewHolder.OnViewHolderClickListener mOnViewHolderClickListener = new BaseViewHolder.OnViewHolderClickListener() {
        @Override
        public void onViewHolderClick(View view, int position) {
            InfoPlayerDialog(mPlayerList.get(position));
        }
    };
}
