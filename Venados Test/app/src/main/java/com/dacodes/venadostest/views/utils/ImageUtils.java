package com.dacodes.venadostest.views.utils;

import com.dacodes.venadostest.R;
import com.nostra13.universalimageloader.core.DisplayImageOptions;

public class ImageUtils {
    public static DisplayImageOptions mDisplayImageOptions = new DisplayImageOptions.Builder().cacheInMemory(true)
            .cacheOnDisc(true).resetViewBeforeLoading(true)
            .showImageForEmptyUri(R.drawable.ic_image_broken)
            .showImageOnFail(R.drawable.ic_default)
            .showImageOnLoading(R.drawable.ic_loading).build();
}
