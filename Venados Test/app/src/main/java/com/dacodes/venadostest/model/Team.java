package com.dacodes.venadostest.model;

import java.util.List;

public class Team {
    public List<Player> forwards;
    public List<Player> centers;
    public List<Player> defenses;
    public List<Player> goalkeepers;
}
