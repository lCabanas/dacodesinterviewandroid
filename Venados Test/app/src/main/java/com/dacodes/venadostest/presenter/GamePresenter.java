package com.dacodes.venadostest.presenter;

import android.content.Context;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.data.service.ClientService;
import com.dacodes.venadostest.data.service.response.GameResponse;
import com.dacodes.venadostest.data.service.response.base.BaseResponse;
import com.dacodes.venadostest.views.viewcontrollers.HomeViewController;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GamePresenter {
    private HomeViewController mViewController;
    private ClientService mClientService;
    private Context mContext;

    public GamePresenter(HomeViewController mViewController, Context mContext) {
        this.mViewController = mViewController;
        this.mContext = mContext;
        if (this.mClientService == null) {
            this.mClientService = new ClientService();
        }
    }

    public void getGamesWS(){
        mViewController.showProgress();
        mClientService
                .getAPI()
                .games()
                .enqueue(new Callback<BaseResponse<GameResponse>>() {
                    @Override
                    public void onResponse(Call<BaseResponse<GameResponse>> call, Response<BaseResponse<GameResponse>> response) {
                        BaseResponse<GameResponse> mResponse = response.body();
                        mViewController.dissmissProgress();
                        if (mResponse != null) {
                            if (mResponse.success && mResponse.data.code == 200) {
                                mViewController.successListWS(mResponse.data.games);
                            } else {
                                mViewController.emptyGames();
                            }
                        } else {
                            mViewController.showMessage(mContext.getString(R.string.message_error));
                            mViewController.emptyGames();
                        }
                    }

                    @Override
                    public void onFailure(Call<BaseResponse<GameResponse>> call, Throwable t) {
                        mViewController.dissmissProgress();
                        mViewController.showMessage(mContext.getString(R.string.message_error));
                        mViewController.emptyGames();
                    }
                });
        mViewController.dissmissProgress();
    }
}
