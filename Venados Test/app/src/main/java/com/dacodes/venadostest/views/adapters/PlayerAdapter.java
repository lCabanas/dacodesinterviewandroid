package com.dacodes.venadostest.views.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.model.Player;
import com.dacodes.venadostest.views.adapters.holders.PlayerHolder;
import com.dacodes.venadostest.views.adapters.holders.base.BaseViewHolder;

import java.util.List;

public class PlayerAdapter extends RecyclerView.Adapter<PlayerHolder> {

    List<Player> mPlayerList;
    private BaseViewHolder.OnViewHolderClickListener mOnViewHolderClickListener;

    public PlayerAdapter(List<Player> mPlayerList, BaseViewHolder.OnViewHolderClickListener mOnViewHolderClickListener) {
        this.mPlayerList = mPlayerList;
        this.mOnViewHolderClickListener = mOnViewHolderClickListener;
    }

    @NonNull
    @Override
    public PlayerHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View master = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_player, parent, false);
        PlayerHolder mHolder = new PlayerHolder(master,mOnViewHolderClickListener);
        return mHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull PlayerHolder holder, int position) {
        Player mItem = getItemAtPosition(position);
        holder.config(mItem);
    }

    @Override
    public int getItemCount() {
        return (mPlayerList == null ? 0 : mPlayerList.size());
    }

    public Player getItemAtPosition(int position) {
        if(position >= 0 && position < mPlayerList.size()){
            return mPlayerList.get(position);
        }
        return null;
    }
}
