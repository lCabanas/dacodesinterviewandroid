package com.dacodes.venadostest.views.adapters.holders;

import android.view.View;
import android.widget.TextView;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.model.Player;
import com.dacodes.venadostest.views.adapters.holders.base.BaseViewHolder;
import com.dacodes.venadostest.views.utils.ImageUtils;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class PlayerHolder extends BaseViewHolder {

    @BindView(R.id.circleImageViewPhoto)
    CircleImageView mCircleImageViewPhoto;

    @BindView(R.id.textViewName)
    TextView mTextViewName;


    public PlayerHolder(View itemView, OnViewHolderClickListener onViewHolderClickListener) {
        super(itemView, onViewHolderClickListener);
        ButterKnife.bind(this, itemView);
    }

    public PlayerHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }


    public void config(Player mPlayer) {
        if (mPlayer.url_image != null && !mPlayer.url_image.isEmpty()) {
            ImageLoader.getInstance().displayImage(mPlayer.url_image, mCircleImageViewPhoto, ImageUtils.mDisplayImageOptions);
        }
        mTextViewName.setText(mPlayer.position + "\n" + mPlayer.getFullName());
    }
}
