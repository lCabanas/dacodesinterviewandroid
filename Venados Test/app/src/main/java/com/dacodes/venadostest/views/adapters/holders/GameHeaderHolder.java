package com.dacodes.venadostest.views.adapters.holders;

import android.view.View;
import android.widget.TextView;

import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter;
import com.dacodes.venadostest.R;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameHeaderHolder extends StickyHeaderGridAdapter.HeaderViewHolder {

    @BindView(R.id.textViewName)
    TextView mTextViewName;

    public GameHeaderHolder(View itemView) {
        super(itemView);
        ButterKnife.bind(this, itemView);
    }

    public void config(String name) {
        mTextViewName.setText(name.toUpperCase());
    }
}
