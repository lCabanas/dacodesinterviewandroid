package com.dacodes.venadostest.views.adapters.holders;

import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.codewaves.stickyheadergrid.StickyHeaderGridAdapter;
import com.dacodes.venadostest.R;
import com.dacodes.venadostest.data.service.ClientService;
import com.dacodes.venadostest.model.Game;
import com.dacodes.venadostest.views.utils.ImageUtils;
import com.dacodes.venadostest.views.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import butterknife.BindView;
import butterknife.ButterKnife;

public class GameHolder extends StickyHeaderGridAdapter.ItemViewHolder {

    public interface OnDateClickListener {
        void onDateClick(Game mGame);
    }

    @BindView(R.id.relativeLayoutDate)
    RelativeLayout mRelativeLayoutDate;

    @BindView(R.id.textViewDate)
    TextView mTextViewDate;

    @BindView(R.id.imageViewLocal)
    ImageView mImageViewLocal;

    @BindView(R.id.textViewLocal)
    TextView mTextViewLocal;

    @BindView(R.id.textViewResult)
    TextView mTextViewResult;

    @BindView(R.id.imageViewVisitor)
    ImageView mImageViewVisitor;

    @BindView(R.id.textViewVisitor)
    TextView mTextViewVisitor;

    OnDateClickListener mOnDateClickListener;

    public GameHolder(View itemView, OnDateClickListener mOnDateClickListener) {
        super(itemView);
        ButterKnife.bind(this, itemView);
        this.mOnDateClickListener = mOnDateClickListener;
    }

    public void config(Game mGame) {
        mTextViewDate.setText(Utils.configureDateDayInt(mGame.datetime) + "\n" + Utils.configureDateDay(mGame.datetime));
        ImageLoader.getInstance().displayImage(ClientService.URL_LOGO, mGame.local ? mImageViewLocal : mImageViewVisitor, ImageUtils.mDisplayImageOptions);
        ImageLoader.getInstance().displayImage(mGame.url_opponent_image, !mGame.local ? mImageViewLocal : mImageViewVisitor, ImageUtils.mDisplayImageOptions);
        mTextViewLocal.setText(mGame.local ? "Venados F.C." : mGame.opponent);
        mTextViewVisitor.setText(!mGame.local ? "Venados F.C." : mGame.opponent);
        mTextViewResult.setText("" + mGame.home_score + " - " + mGame.away_score);
        mRelativeLayoutDate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mOnDateClickListener.onDateClick(mGame);
            }
        });
    }
}
