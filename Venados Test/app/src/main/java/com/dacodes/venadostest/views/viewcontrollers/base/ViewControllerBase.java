package com.dacodes.venadostest.views.viewcontrollers.base;

import java.util.List;

public interface ViewControllerBase<T> {
    void showMessage(String message);

    void successObjctWS(T object);

    void successListWS(List<T> listObject);

    void showProgress();

    void dissmissProgress();
}
