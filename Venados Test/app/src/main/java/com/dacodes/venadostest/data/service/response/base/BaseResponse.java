package com.dacodes.venadostest.data.service.response.base;
public class BaseResponse<T> {

    public BaseResponse() {
    }

    public boolean success;
    public T data;
}
