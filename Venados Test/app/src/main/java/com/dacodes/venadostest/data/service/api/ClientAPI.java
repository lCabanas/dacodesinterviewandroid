package com.dacodes.venadostest.data.service.api;

import com.dacodes.venadostest.data.service.response.GameResponse;
import com.dacodes.venadostest.data.service.response.TeamResponse;
import com.dacodes.venadostest.data.service.response.base.BaseResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Headers;

public interface ClientAPI {

    @Headers({"Accept: application/json"})
    @GET("games")
    Call<BaseResponse<GameResponse>> games();

    @Headers({"Accept: application/json"})
    @GET("players")
    Call<BaseResponse<TeamResponse>> players();
}
