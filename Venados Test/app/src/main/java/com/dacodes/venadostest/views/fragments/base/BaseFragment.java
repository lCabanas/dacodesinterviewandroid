package com.dacodes.venadostest.views.fragments.base;

import android.content.Context;
import android.widget.Toast;

import androidx.fragment.app.Fragment;

public class BaseFragment extends Fragment {

    public void showToastMessage(String mensaje, Context mContext){
        Toast.makeText(mContext,mensaje, Toast.LENGTH_SHORT).show();
    }

    public void showToastLongMessage(String mensaje, Context mContext){
        Toast.makeText(mContext,mensaje, Toast.LENGTH_LONG).show();
    }
}
