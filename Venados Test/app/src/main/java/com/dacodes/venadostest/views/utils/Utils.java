package com.dacodes.venadostest.views.utils;

import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {
    public static String configureDate(Date mDate) {
        String fecha = null;
        SimpleDateFormat mDateFormat = new SimpleDateFormat("dd/MM/yyyy");
        try {

            fecha = mDateFormat.format(mDate);

        } catch (Exception e) {

        }
        return fecha;
    }

    public static String configureDateDay(Date mDate) {
        String fecha = null;
        SimpleDateFormat mDateFormat = new SimpleDateFormat("EEE");
        try {

            fecha = mDateFormat.format(mDate);

        } catch (Exception e) {

        }
        return fecha;
    }

    public static String configureDateDayInt(Date mDate) {
        String fecha = null;
        SimpleDateFormat mDateFormat = new SimpleDateFormat("dd");
        try {

            fecha = mDateFormat.format(mDate);

        } catch (Exception e) {

        }
        return fecha;
    }

    public static String configureDateMonth(Date mDate) {
        String fecha = null;
        SimpleDateFormat mDateFormat = new SimpleDateFormat("MMMM");
        try {

            fecha = mDateFormat.format(mDate);

        } catch (Exception e) {

        }
        return fecha;
    }

}
