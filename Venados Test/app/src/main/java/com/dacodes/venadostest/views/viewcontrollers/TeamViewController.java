package com.dacodes.venadostest.views.viewcontrollers;

import com.dacodes.venadostest.model.Team;
import com.dacodes.venadostest.views.viewcontrollers.base.ViewControllerBase;

public interface TeamViewController extends ViewControllerBase<Team> {
    void emptyTeam();
}
