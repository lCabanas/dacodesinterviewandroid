package com.dacodes.venadostest.views.viewcontrollers;

import com.dacodes.venadostest.model.Game;
import com.dacodes.venadostest.views.viewcontrollers.base.ViewControllerBase;

public interface HomeViewController extends ViewControllerBase<Game> {
    void emptyGames();
}
