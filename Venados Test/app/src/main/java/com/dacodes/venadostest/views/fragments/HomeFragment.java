package com.dacodes.venadostest.views.fragments;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentStatePagerAdapter;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;
import androidx.viewpager.widget.ViewPager;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.data.service.ClientService;
import com.dacodes.venadostest.model.Game;
import com.dacodes.venadostest.presenter.GamePresenter;
import com.dacodes.venadostest.views.fragments.base.BaseFragment;
import com.dacodes.venadostest.views.utils.ImageUtils;
import com.dacodes.venadostest.views.viewcontrollers.HomeViewController;
import com.google.android.material.tabs.TabLayout;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeFragment extends BaseFragment implements HomeViewController {
    @BindView(R.id.tabs)
    TabLayout mTabLayout;

    @BindView(R.id.viewPagerGame)
    ViewPager mViewPager;

    @BindView(R.id.swipeRefreshGame)
    SwipeRefreshLayout mSwipeRefreshLayout;

    @BindView(R.id.image_header)
    ImageView mImageViewHeader;

    HomePagerAdapter mHomePagerAdapter;
    GamePresenter mGamePresenter;
    int currentFragment = 0;

    List<String> listTabs = new ArrayList<>();
    List<Game> mGameList = new ArrayList<>();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            view = inflater.inflate(R.layout.fragment_home, container, false);
            ButterKnife.bind(this, view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    @Override
    public void onStart() {
        super.onStart();
        ImageLoader.getInstance().displayImage(ClientService.URL_LOGO, mImageViewHeader, ImageUtils.mDisplayImageOptions);

        if (mGamePresenter == null) {
            mGamePresenter = new GamePresenter(this, getActivity());
        }
        mGamePresenter.getGamesWS();
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                currentFragment = mViewPager != null ? mViewPager.getCurrentItem() : 0;
                mHomePagerAdapter = null;
                mGamePresenter.getGamesWS();
            }
        });

    }

    private void configureSectionPager() {

        if (mHomePagerAdapter == null) {
            mHomePagerAdapter = new HomePagerAdapter(getFragmentManager(), getActivity());
            mViewPager.setAdapter(mHomePagerAdapter);
            mTabLayout.setupWithViewPager(mViewPager);
        }
        mViewPager.setCurrentItem(currentFragment);
    }

    @Override
    public void emptyGames() {

    }

    @Override
    public void showMessage(String message) {
        showToastMessage(message, getActivity());
    }

    @Override
    public void successObjctWS(Game object) {

    }

    @Override
    public void successListWS(List<Game> listObject) {

        if (listObject != null && !listObject.isEmpty()) {
            this.mGameList = listObject;
            for (Game mGame : listObject) {
                if (!listTabs.contains(mGame.league)) {
                    listTabs.add(mGame.league);
                }
            }
            Collections.sort(listTabs);
            configureSectionPager();
        }
    }

    @Override
    public void showProgress() {
        mSwipeRefreshLayout.setRefreshing(true);
    }

    @Override
    public void dissmissProgress() {
        mSwipeRefreshLayout.setRefreshing(false);
    }

    private List<Game> getGameByLeague(List<Game> mGameList, String league){
        List<Game> mGameListFilter = new ArrayList<>();
        for(Game mGame : mGameList){
            if(mGame.league.equals(league)){
                mGameListFilter.add(mGame);
            }
        }
        return mGameListFilter;
    }

    private class HomePagerAdapter extends FragmentStatePagerAdapter {
        Context mContext;

        public HomePagerAdapter(FragmentManager fm, Context mContext) {
            super(fm);
            this.mContext = mContext;
        }

        @NonNull
        @Override
        public Fragment getItem(int position) {
            return GameFragment.newInstance(mContext, getGameByLeague(mGameList, listTabs.get(position)));
        }

        @Override
        public int getCount() {
            return listTabs.size();
        }

        @Nullable
        @Override
        public CharSequence getPageTitle(int position) {
            return listTabs.get(position).toUpperCase();
        }
    }
}