package com.dacodes.venadostest.views.fragments;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.provider.CalendarContract;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.TextView;

import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.dacodes.venadostest.R;
import com.dacodes.venadostest.model.Game;
import com.dacodes.venadostest.model.Player;
import com.dacodes.venadostest.views.adapters.GameAdapter;
import com.dacodes.venadostest.views.adapters.PlayerAdapter;
import com.dacodes.venadostest.views.adapters.holders.GameHolder;
import com.dacodes.venadostest.views.adapters.holders.base.BaseViewHolder;
import com.dacodes.venadostest.views.fragments.base.BaseFragment;
import com.dacodes.venadostest.views.utils.ImageUtils;
import com.dacodes.venadostest.views.utils.Utils;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;

public class GameFragment extends BaseFragment {
    @BindView(R.id.recyclerViewGames)
    RecyclerView mRecyclerViewGames;

    @BindView(R.id.textViewEmpty)
    TextView mTextViewEmpty;

    Context mContext;
    List<Game> mGameList;

    public static GameFragment newInstance(Context mContext, List<Game> mGameList) {
        GameFragment mFragment = new GameFragment();
        mFragment.mGameList = mGameList;
        mFragment.mContext = mContext;
        return mFragment;
    }

    @Override
    public void onStart() {
        super.onStart();
        configureRecyclerView(mGameList);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = null;
        if (view != null) {
            ViewGroup parent = (ViewGroup) view.getParent();
            if (parent != null)
                parent.removeView(view);
        }

        try {
            view = inflater.inflate(R.layout.fragment_game, container, false);
            ButterKnife.bind(this, view);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return view;
    }

    public void configureRecyclerView(List<Game> mGameList) {
        if(mGameList != null && !mGameList.isEmpty()) {
            GameAdapter mGameAdapter = new GameAdapter(mGameList, mOnDateClickListener);
            LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
            mRecyclerViewGames.setLayoutManager(layoutManager);
            mRecyclerViewGames.setHasFixedSize(true);
            mRecyclerViewGames.setItemAnimator(new DefaultItemAnimator());
            mRecyclerViewGames.setAdapter(mGameAdapter);
            mTextViewEmpty.setVisibility(View.GONE);
        }else{
            mTextViewEmpty.setVisibility(View.VISIBLE);
        }
    }

    GameHolder.OnDateClickListener mOnDateClickListener = new GameHolder.OnDateClickListener() {
        @Override
        public void onDateClick(Game mGame) {
            Intent calIntent = new Intent(Intent.ACTION_INSERT);
            calIntent.setType("vnd.android.cursor.item/event");
            calIntent.putExtra(CalendarContract.Events.TITLE, "Juego " + mGame.league + " Venados F.C. VS " + mGame.opponent);
            calIntent.putExtra(CalendarContract.Events.EVENT_LOCATION, mGame.local ? "Partido Local" : "Partido Visitante");
            calIntent.putExtra(CalendarContract.Events.DESCRIPTION, "Venados F.C. VS " + mGame.opponent);

            Calendar calDate = new GregorianCalendar();
            calDate.setTime(mGame.datetime);
            calIntent.putExtra(CalendarContract.EXTRA_EVENT_ALL_DAY, true);
            calIntent.putExtra(CalendarContract.EXTRA_EVENT_BEGIN_TIME,
                    calDate.getTimeInMillis());
            calIntent.putExtra(CalendarContract.EXTRA_EVENT_END_TIME,
                    calDate.getTimeInMillis());

            startActivity(calIntent);
        }
    };

}
