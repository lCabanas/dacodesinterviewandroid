package com.dacodes.venadostest.model;

import com.google.gson.annotations.SerializedName;

import java.util.Date;

public class Player {

    public String name;

    public String first_surname;

    public String second_surname;

    public Date birthday;

    public String birth_place;

    public float weight;

    public float height;

    public String position;

    public int number;

    public String position_short;

    public  String last_team;

    @SerializedName("image")
    public String url_image;

    public String getFullName(){
        return name + " " + first_surname + " " + second_surname;
    }
}
